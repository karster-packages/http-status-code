<?php

namespace karster\response\tests;

use karster\response\HttpStatusCode;
use PHPUnit\Framework\TestCase;
use \Exception;

class PasswordSimilarityTest extends TestCase
{
    private const INVALID_CODE = 800;

    /**
     * @throws Exception
     */
    public function testGetHttpHeaderFor()
    {
        $this->assertSame("HTTP/1.1 100 Continue", HttpStatusCode::getHttpHeaderFor(HttpStatusCode::CONTINUE));
    }

    /**
     * @throws Exception
     */
    public function testGetMessageFor()
    {
        $this->assertSame("403 Forbidden", HttpStatusCode::getMessageFor(HttpStatusCode::FORBIDDEN));
    }

    /**
     * @throws Exception
     */
    public function testIsErrorCode()
    {
        $this->assertTrue(HttpStatusCode::isErrorCode(HttpStatusCode::BAD_GATEWAY));
    }

    /**
     * @throws Exception
     */
    public function testIsNotErrorCode()
    {
        $this->assertFalse(HttpStatusCode::isErrorCode(HttpStatusCode::OK));
    }

    public function testIsNotValidCode()
    {
        $this->assertFalse(HttpStatusCode::isValidCode(static::INVALID_CODE));
    }

    public function testCanHaveBody()
    {
        $this->assertTrue(HttpStatusCode::canHaveBody(HttpStatusCode::OK));
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testGetHttpHeaderForException()
    {
        $this->expectException(\Exception::class);
        
        HttpStatusCode::getHttpHeaderFor(static::INVALID_CODE);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testGetMessageForException()
    {
        $this->expectException(\Exception::class);

        HttpStatusCode::getMessageFor(static::INVALID_CODE);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testIsErrorCodeException()
    {
        $this->expectException(\Exception::class);

        HttpStatusCode::isErrorCode(static::INVALID_CODE);
    }
}
