<?php

namespace karster\response;

use \Exception;

/**
 * Class HttpStatusCode
 * @package karster\response
 */
final class HttpStatusCode
{
    public const CONTINUE = 100;
    public const SWITCHING_PROTOCOLS = 101;
    public const OK = 200;
    public const CREATED = 201;
    public const ACCEPTED = 202;
    public const NONAUTHORITATIVE_INFORMATION = 203;
    public const NO_CONTENT = 204;
    public const RESET_CONTENT = 205;
    public const PARTIAL_CONTENT = 206;
    public const MULTIPLE_CHOICES = 300;
    public const MOVED_PERMANENTLY = 301;
    public const FOUND = 302;
    public const SEE_OTHER = 303;
    public const NOT_MODIFIED = 304;
    public const USE_PROXY = 305;
    public const UNUSED = 306;
    public const TEMPORARY_REDIRECT = 307;
    public const BAD_REQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const PAYMENT_REQUIRED = 402;
    public const FORBIDDEN = 403;
    public const NOT_FOUND = 404;
    public const METHOD_NOT_ALLOWED = 405;
    public const NOT_ACCEPTABLE = 406;
    public const PROXY_AUTHENTICATION_REQUIRED = 407;
    public const REQUEST_TIMEOUT = 408;
    public const CONFLICT = 409;
    public const GONE = 410;
    public const LENGTH_REQUIRED = 411;
    public const PRECONDITION_FAILED = 412;
    public const REQUEST_ENTITY_TOO_LARGE = 413;
    public const REQUEST_URI_TOO_LONG = 414;
    public const UNSUPPORTED_MEDIA_TYPE = 415;
    public const REQUESTED_RANGE_NOT_SATISFIABLE = 416;
    public const EXPECTATION_FAILED = 417;
    public const INTERNAL_SERVER_ERROR = 500;
    public const NOT_IMPLEMENTED = 501;
    public const BAD_GATEWAY = 502;
    public const SERVICE_UNAVAILABLE = 503;
    public const GATEWAY_TIMEOUT = 504;
    public const VERSION_NOT_SUPPORTED = 505;

    /**
     * @return array
     */
    private static function getMessages(): array
    {
        return [
            static::CONTINUE => '100 Continue',
            static::SWITCHING_PROTOCOLS => '101 Switching Protocols',
            static::OK => '200 OK',
            static::CREATED => '201 Created',
            static::ACCEPTED => '202 Accepted',
            static::NONAUTHORITATIVE_INFORMATION => '203 Non-Authoritative Information',
            static::NO_CONTENT => '204 No Content',
            static::RESET_CONTENT => '205 Reset Content',
            static::PARTIAL_CONTENT => '206 Partial Content',
            static::MULTIPLE_CHOICES => '300 Multiple Choices',
            static::MOVED_PERMANENTLY => '301 Moved Permanently',
            static::FOUND => '302 Found',
            static::SEE_OTHER => '303 See Other',
            static::NOT_MODIFIED => '304 Not Modified',
            static::USE_PROXY => '305 Use Proxy',
            static::UNUSED => '306 (Unused)',
            static::TEMPORARY_REDIRECT => '307 Temporary Redirect',
            static::BAD_REQUEST => '400 Bad Request',
            static::UNAUTHORIZED => '401 Unauthorized',
            static::PAYMENT_REQUIRED => '402 Payment Required',
            static::FORBIDDEN => '403 Forbidden',
            static::NOT_FOUND => '404 Not Found',
            static::METHOD_NOT_ALLOWED => '405 Method Not Allowed',
            static::NOT_ACCEPTABLE => '406 Not Acceptable',
            static::PROXY_AUTHENTICATION_REQUIRED => '407 Proxy Authentication Required',
            static::REQUEST_TIMEOUT => '408 Request Timeout',
            static::CONFLICT => '409 Conflict',
            static::GONE => '410 Gone',
            static::LENGTH_REQUIRED => '411 Length Required',
            static::PRECONDITION_FAILED => '412 Precondition Failed',
            static::REQUEST_ENTITY_TOO_LARGE => '413 Request Entity Too Large',
            static::REQUEST_URI_TOO_LONG => '414 Request-URI Too Long',
            static::UNSUPPORTED_MEDIA_TYPE => '415 Unsupported Media Type',
            static::REQUESTED_RANGE_NOT_SATISFIABLE => '416 Requested Range Not Satisfiable',
            static::EXPECTATION_FAILED => '417 Expectation Failed',
            static::INTERNAL_SERVER_ERROR => '500 Internal Server Error',
            static::NOT_IMPLEMENTED => '501 Not Implemented',
            static::BAD_GATEWAY => '502 Bad Gateway',
            static::SERVICE_UNAVAILABLE => '503 Service Unavailable',
            static::GATEWAY_TIMEOUT => '504 Gateway Timeout',
            static::VERSION_NOT_SUPPORTED => '505 HTTP Version Not Supported'
        ];
    }

    /**
     * @param int $code
     * @return string
     * @throws Exception
     */
    public static function getHttpHeaderFor(int $code): string
    {
        if (static::isValidCode($code)) {
            return 'HTTP/1.1 ' . static::getMessages()[$code];
        }

        throw new Exception('Invalid HTTP status code');
    }

    /**
     * @param int $code
     * @return string
     * @throws Exception
     */
    public static function getMessageFor(int $code): string
    {
        if (static::isValidCode($code)) {
            return static::getMessages()[$code];
        }

        throw new Exception('Invalid HTTP status code');
    }

    /**
     * @param int $code
     * @return bool
     * @throws Exception
     */
    public static function isErrorCode(int $code): bool
    {
        if (static::isValidCode($code)) {
            return is_numeric($code) && $code >= static::BAD_REQUEST;
        }

        throw new Exception('Invalid HTTP status code');
    }

    /**
     * @param int $code
     * @return bool
     */
    public static function isValidCode(int $code): bool
    {
        return isset(static::getMessages()[$code]);
    }

    /**
     * @param int $code
     * @return bool
     */
    public static function canHaveBody(int $code): bool
    {
        return
            ($code < static::CONTINUE || $code >= static::OK) &&
            $code != static::NO_CONTENT &&
            $code != static::NOT_MODIFIED;
    }
}
