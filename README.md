# HTTP Status Code


## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```shell
composer require karster/http-status-code:"*"
```

or add

```
"karster/http-status-code": "*"
```

to the require section of your composer.json.

## Usage

```php
use karster\response\HttpStatusCode;

$message = HttpStatusCode::getMessageFor(HttpStatusCode::HTTP_OK);

```

## Tests

```
./vendor/bin/phpunit -c phpunit.xml
```

## Contribution
Have an idea? Found a bug? See [how to contribute][contributing].

## License
MIT see [LICENSE][] for the full license text.

[license]: LICENSE.md
[contributing]: CONTRIBUTING.md